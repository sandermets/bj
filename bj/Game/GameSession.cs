﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace blackjack.Game
{
    public class GameSession
    {
        private BlackJack _blackJack;

        public BlackJack BlackJack
        {
            get { return this._blackJack; }
            set { this._blackJack = value; }
        }

        public GameSession()
        {
            if (HttpContext.Current.Session["TheGame"] == null)
            {
                HttpContext.Current.Session["TheGame"] = new BlackJack();
            } 
            this.BlackJack = (BlackJack) HttpContext.Current.Session["TheGame"];
        }

        public void Start()
        {
            this.BlackJack.Money -= 10;
            this.BlackJack.Cards.Clear();

            //start refill the cards
            DeckOfCards doc = new DeckOfCards();
            this.BlackJack.Cards = doc.Cards;
            //end refill the cards

            this.BlackJack.Selected.Clear();
            this.BlackJack.CurrentPoints.Clear();
            this.BlackJack.GameMessage = "";

            this.BlackJack.BtnStart = true;
            this.BlackJack.BtnBreak = this.BlackJack.BtnDeal = false;
        }

        public void Deal()
        {
            this.BlackJack.RandomSelect();
        }

        public void Break()
        {
            this.BlackJack.BreakNotActive = false;
            if (this.BlackJack.Selected.Count < 2)
            {
                this.BlackJack.Money += 10;
            }
            else
            {
                this.BlackJack.Money += 5;
            }
            while (this.BlackJack.GameMessage == "")
            {
                this.Deal();
            }
            this.BlackJack.BreakNotActive = true;
        }
    }
}