﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using blackjack.Game;

namespace blackjack.Controllers
{

    public class ValuesController : ApiController
    {
        // GET api/values/start
        [HttpGet]
        [ActionName("Start")]
        public BlackJack Start()
        {
            GameSession gs = new GameSession();
            gs.Start();
            return gs.BlackJack;
        }

        // GET api/values/deal
        [HttpGet]
        [ActionName("Deal")]
        public BlackJack Deal()
        {
            GameSession gs = new GameSession();
            gs.Deal();
            return gs.BlackJack;
        }

        // GET api/values/break
        [HttpGet]
        [ActionName("Break")]
        public BlackJack Break()
        {
            GameSession gs = new GameSession();
            gs.Break();
            return gs.BlackJack;
        }

    }
}
