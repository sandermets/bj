# BlackJack #

Project in live can be reached via URL http://sandermets-001-site1.smarterasp.net/  

## Rules ##

* If You hit 21 Your recive 30 credits 
* If You break game having exactly one card, You do not loose anything and game will be played to finish
* However, if You break the game after having two or more cards You loose half of the bet
* You loose hole bet if You have more than 21 points  

## Game algorithm ##

Is basic - sum of points. Only thing is that aces have two different points, so most exceptional case is 4 aces, which leads us into following 5 combinations:  
1 + 1 + 1 + 1  
1 + 1 + 1 + 11  
1 + 1 + 11 + 11  
1 + 11 + 11 + 11  
11 + 11 + 11 + 11  

That also means possible different scores at the same time. We do not take into account scores over 21 points.  

First game was done in frontend using Javascript(Angular). Can be seen here [https://bitbucket.org/sandermets/firsttryfbtsson/](https://bitbucket.org/sandermets/firsttryfbtsson/)   Current version is data driven approach wannabe :)  
All the logic is calculated in server, and front end states are controlled by back-end over Web API.

## Soft design ##
Design is somewhat flawed, because REST api itself is stateless, solution uses session.  

## TODO ##
There is bug regarding to win situation, if 21 is not in the first position of results, the points will not be shown   
Figure out the solution without using session  
Create Entities of cards and related points